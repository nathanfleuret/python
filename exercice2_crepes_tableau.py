#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""A partir du dictionnaire "recette" fourni qui contient la liste d'ingrédient requis pour "une crêpe" sous forme de dictionnaire,

Vous écrirez 2 fonctions :

"recette_pour" qui prendra en paramètre un nombre de crêpes
"recette_personne" qui prendra en paramètre un nombre de personnes"""

recette = {"farine" : {"quantite": 20 , "unite": "g" },
       "oeuf" : {"quantite": 0.2, "unite": ""},
       "lait" : { "quantite": 0.05 , "unite": "L"},
       "huile": { "quantite": 0.001 , "unite": "L"},
       "rhum": { "quantite": 0.001, "unite": "L" }}

def recette_pour(nb_crepe) :
    print("Pour faire "+ str(nb_crepe) + " crêpes, il faut :")
    print("- "+str(int(recette["farine"]["quantite"])*nb_crepe)+" "+recette["farine"]["unite"]+" de farine")
    print("- "+str(float(recette["lait"]["quantite"])*nb_crepe)+" "+recette["lait"]["unite"]+" de lait")
    print("- "+str(float(recette["rhum"]["quantite"])*nb_crepe)+" "+recette["rhum"]["unite"]+" de rhum")
    print("- "+str(float(recette["oeuf"]["quantite"])*nb_crepe)+recette["oeuf"]["unite"]+" de oeuf")
    print("- "+str(float(recette["huile"]["quantite"])*nb_crepe)+" "+recette["huile"]["unite"]+" de huile")

def recette_personne(nb_personne):
    print("Recette des crêpes pour " + str(nb_personne) + " (5 crêpes/personne)\n")
    recette_pour(5 * nb_personne)

print("Aujourd'hui c'est crêpe party !")
retour = ""

while retour != "y" :
    retour = input("Connaissez-vous le nombre de crêpes à préparer ? (y/n) ")
    if retour == "y" :
        recette_pour(int(input("Combien voulez-vous de crêpes ?\n")))
    else :
        retour = input("Voulez-vous préparer les crêpes selon un nombre de personne ? (y/n) ")
        if retour == "y" :
            recette_personne(int(input("Pour combien de personnes voulez-vous cuisiner ?\n")))