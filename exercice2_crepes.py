#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""A partir du dictionnaire "recette" fourni qui contient la liste d'ingrédient requis pour "une crêpe" sous forme de dictionnaire,

Vous écrirez 2 fonctions :

"recette_pour" qui prendra en paramètre un nombre de crêpes
"recette_personne" qui prendra en paramètre un nombre de personnes"""

recette = {"farine" : {"quantite": 20 , "unite": "g" },
       "oeuf" : {"quantite": 0.2, "unite": ""},
       "lait" : { "quantite": 0.05 , "unite": "L"},
       "huile": { "quantite": 0.001 , "unite": "L"},
       "rhum": { "quantite": 0.001, "unite": "L" }}

def recette_pour(nb_crepe) :
    print("Pour faire "+ str(nb_crepe) + " crêpes, il faut :")
    for ingredient, volume in recette.items():
        ligne = "- "
        for cle, valeur in volume.items():
            if cle == "quantite":
                ligne += str(float(valeur) * nb_crepe)
            else :
                ligne += " " + valeur + " de "
        print(ligne + ingredient)

def recette_personne(nb_personne):
    print("Recette des crêpes pour " + str(nb_personne) + " (5 crêpes/personne)\n")
    recette_pour(5 * nb_personne)

print("Aujourd'hui c'est crêpe party !")
retour = ""

while retour != "y" :
    retour = input("Connaissez-vous le nombre de crêpes à préparer ? (y/n) ")
    if retour == "y" :
        recette_pour(int(input("Combien voulez-vous de crêpes ?\n")))
    else :
        retour = input("Voulez-vous préparer les crêpes selon un nombre de personne ? (y/n) ")
        if retour == "y" :
            recette_personne(int(input("Pour combien de personnes voulez-vous cuisiner ?\n")))