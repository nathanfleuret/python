#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""PREMIER PETIT PROGRAMME
En reprenant ce que nous venons de voir

Nous allons écrire un programme qui :

Demande à l'utilisateur de saisir 2 variables au clavier (fonction input())
Tester ces variables afin de déterminer si ce sont des nombres décimaux (fonction isinstance())
(attention à bien gérer les retours des erreurs afin de ne pas planter le programme)
Tant que ces variables génère des erreurs (ne sont pas des nombres décimaux) nous afficherons des messages à l'utilisateur l'invitant à ressayer
Nous comparerons ces 2 variables de type float afin de dire à l'utilisateur si la première est :
supérieur à la seconde
inférieure à la seconde
égale à la seconde"""

a, b = None, None
while isinstance(a, float) != True and isinstance(b, float) != True:
    print("Veuillez saisir 2 valeurs numériques")
    print("Nous allons comparer A et B")
    try:
        a = float(input("Saisir le premier nombre (A): "))
        b = float(input("Saisir le premier nombre (B): "))
    except ValueError:
        print("Saisir une valeur numérique !")
    except TypeError:
        print("Saisir une valeur numérique !")
    except NameError:
        print("Saisir une valeur numérique !")
    finally:
        print("A = "+str(a))
        print("B = "+str(b))

if a > b:
    print(str(a)+" est supérieur à "+str(b))
elif a < b:
    print(str(a)+" est inférieur à "+str(b))
elif a == b:
    print(str(a)+" est égale à "+str(b))
else:
    print("what else?")