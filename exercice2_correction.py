#!/usr/bin/env python
# -*- coding: utf-8 -*-

## Exercice dictionnaire

# Enonce
"""A partir du dictionnaire "recette" fourni qui contient la liste d'ingrédient
requis pour "une crêpe" sous forme de dictionnaire,
Vous écrirez 2 fonctions :
* "recette_pour" qui prendra en paramètre un nombre de crêpes
* "recette_personne" qui prendra en paramètre un nombre de personnes"""

recette = {"farine" : {"quantite": 20 , "unite": "g" },
           "oeuf" : {"quantite": 0.2, "unite": ""},
           "lait" : { "quantite": 0.05 , "unite": "L"},
           "huile": { "quantite": 0.001 , "unite": "L"},
           "rhum": { "quantite": 0.001, "unite": "L" }}
print(recette)
print(type(recette))

# Correction

print("Pour faire une crepe, il faut")

def recette_pour(nb_crepe=5):
    print("Pour faire "+str(nb_crepe)+" crêpes, il faut :")
    for ingredient, quantite in recette.items():
        print("- {} {} de {}".format(list(quantite.values())[0]*nb_crepe, list(quantite.values())[1], ingredient))

def recette_personne(nb_personne=1):
    print("\nRecette des crêpes pour "+str(nb_personne)+" (5 crêpes/personne) \n")
    recette_pour(nb_personne*5)

recette_pour(5)
recette_personne(4)



