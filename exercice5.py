#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""EXERCICE 5 : API, BASE DE DONNÉE, GUI
CONTEXTE
L'objectif de cet exercice étant d'interagir avec une API, une base de donnée, et une interface utilisateur restituant les informations en base de donnée de façon "visuelle".

ENONCÉ
Vous utiliserez l'API de coingecko (public sans inscription, ni clé API) afin d'extraire le cours du "BTC / EURO", vous stockerez cette information dans une base de donnée SQLite3 et capturerez un point de donnée toutes les 10min

Vous trouverez un module python vous permettant de générer un graphique de l'évolution du cours du bitcoin dans la durée, ce graphique devra s'actualiser lors de chaque nouveaux points de donnée.

PISTES DE RECHERCHES
Doc API coingecko : https://www.coingecko.com/api/documentations/v3

Source de l'info chez coingecko : https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=eur

Requests permet d'aller récupérer le contenu de cette URL

Le contenu est sous forme de JSON (dictionnaire?)

Pour les interfaces générant un graphique voir par exemple : https://plot.ly/python/time-series/"""

# on importe les modules requis
import requests
import sqlite3
import json
import time

# on connecte la BDD
conn = sqlite3.connect('BDD.db')
c = conn.cursor()

# on requête sur l'url
result = requests.get("https://api.coingecko.com/api/v3/simple/price?ids=bitcoin&vs_currencies=eur")
timestamp = time.time()
contenu = result.content

# on convertit le json reçu en dictionnaire
dictionnaire = json.loads(contenu)

# on récupère l'info dans le dictionnaire
valeur = dictionnaire["bitcoin"]["eur"]

ajout = (timestamp, valeur,)
print(ajout)
print(type(ajout))

#ajout = (1,1,)
c.execute("INSERT INTO bitcoin (date, valeur) VALUES(?,?)", ajout)

# on sauvegarde et on ferme la BDD
conn.commit()
conn.close()