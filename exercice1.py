#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""EXERCICE 1 : IF, ELIF, ELSE, VAR, DIVISION
ENONCÉ
Vous allez écrire un programme qui testera si une année est bissextile ou non ?

Le paramètre de l'année à tester devra être obtenu via saisie clavier de l'utilisateur.

Le programme n'a pas besoin de plus de 2 variables, pas d'exigeance sur la gestion 
des exceptions on part du principe que l'utilisateur saisi au clavier un nombre entier positif."""

annee = int(input("Saisir une année: "))
if annee % 400 == 0 or (annee % 4 == 0 and annee % 100 != 0) :
    print("L'année " + str(annee) + " est bissextile")
else :
    print("L'année "+ str(annee) + " n'est pas bissextile") 