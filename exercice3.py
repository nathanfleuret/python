#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""EXERCICE 3 : WEB-SCRAPER
ENONCÉ
Vous devrez récupérer toutes les images d'un site "wordpress" donné, pour se faire vous devrez faire appel à différents modules permettant de communiquer avec un serveur web via HTTP (urllib ? request ?) et aussi parser du HTML (BeautifulSoup4 ?) ou encore enregistrer les images sous formes de fichier (os ?).

Les images tel que bannière ou logo ne devront pas être récupérer uniquement les images contenus dans les articles

L'URL du site mirroir sera hébergé sur le PC de l'intervenant afin d'éviter depuis une même connexion d'aller attaquer un site internet qui prendrait alors beaucoup de connexion en provenance d'une même IP publique dans le cadre de cet exercice depuis le réseau du CESI

Merci en cas d'absence du mirroir, de ne pas utiliser le réseau CESI pour reproduire cet exercice (4G ou internet "perso")"""

# on importe les modules requis
import requests
from bs4 import BeautifulSoup
import time

# Debut du decompte du temps
debut_timer = time.time()

# on requete le site à scrapper

url_site = "http://10.176.128.142"
id_page = 1
page = url_site
nb_image = 0
while page: 
    result = requests.get(page)

    # on teste la connexion
    #print(result.status_code)
    # doit renvoyer 200

    contenu = result.content
    soupe = BeautifulSoup(contenu)
    images = soupe.find_all("img")

    # je stoppe la boucle si aucune image n'est présente sur la page
    if images == []:
        print(nb_image)
        break
    
    for image in images: 
        nb_image += 1
        image_url = image.get('src')
        image_name = image_url.split("/")[-1]
        image_file = requests.get(image_url)
        """if isinstance(image_file.content, bytes) != True:
            print(type(image_file.content))
            print(image_name)
        if image_file.status_code != 200:
            print(image_name)"""
        open("D:/Documents/CESi/Python/Exercices/downloads/" + image_name, 'wb').write(image_file.content)

    id_page += 1
    page = url_site + "/page/" + str(id_page)

# Affichage du temps d execution
print("Temps d execution : %s secondes ---" % (time.time() - debut_timer))